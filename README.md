GitLab
August 30, 2022
Re: Paralegal (Corporate)

Dear GitLab Team:

The attached resume is in response to the vacancy for a Paralegal (Corporate) with GitLab. As an experienced paralegal looking to join a passionate team that helps people around the world, this position would be a great fit for me. I have over seven years of experience working in a law firm providing paralegal support across corporate, litigation, and labor and employment practice groups. GitLab’s dedication to transparency and collaboration specifically caught my interest, as these are values in my daily professional and personal life.

As a self-motivated and results-driven individual, I am excited to share my skills with a new team.  My day-to-day environment requires prioritizing various projects, fast-paced multitasking while maintaining accuracy and confidentiality, and working cross-functionally with various management and staff personnel. Strong interpersonal communication skills, liaising between departments, conflict management, and my ability to work in both a team setting and on self-driven projects are just a few of my strengths.

After a brief introductory period at my previous employer as Receptionist, I was promoted to Administrative Assistant and, after hitting targets and exceeding expectations year after year in that role, I was again promoted to my Paralegal, which I did for 4 years. I became more involved with litigation and recently took a Paralegal position with Greenberg Traurig,  where I have been for nearly a year now. 
In this role, I draft and revise pleadings, official correspondence, and memoranda, as well as analyze data and evaluate information to be produced in discovery. I maintain calendars and dockets for the entire department; bluebook and cite-check on Lexis and Westlaw; and research state and federal rules and procedures.

For the past eight years, I have been in a professional environment that has given me all the necessary skills you are looking for, including attention to detail, discretion, efficiency, and effective problem solving. I also bring personal integrity, accountability, and a drive to meet targets and deadlines to the role. 

I look forward to the opportunity to prove myself as a member of the GitLab team.

Thank you for your consideration.

Very truly yours,
Valentina Capone
